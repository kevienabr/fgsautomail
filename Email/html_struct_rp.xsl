<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" indent="yes" encoding="UTF-8" />
<xsl:template match="/Rows">
 <html>
	<head>
	<title>DATA RP</title>
	<style>
		table {
			width:40%;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		td, th {
			padding: 5px;
			text-align: center;
		}
	</style>
 	</head>
  	<body>
    	<h3>Data Auto Post Email FGS</h3>
		<h5><i>Report Production</i></h5>
    	<table border="1" border-collapse="collapse">
      		<tr bgcolor="lightgrey">
				<th align="center" >MILL</th>
        		<th align="center">FFBRCVD</th>
        		<th align="center">FFBPRCD</th>
				<th align="center">BALANCE</th>
				<th align="center">OER</th>
				<th align="center" width="1000">KER</th>
        		<th align="center">RAINFALL</th>
				<th align="center">MILLSTART</th>
				<th align="center">MILLEND</th>
				<th align="center">THROUGHPUT</th>
      		</tr>
      	<xsl:apply-templates select="Row">
      	<xsl:sort select="title" />
      	</xsl:apply-templates>
    	</table>	
  	</body>
  </html>
</xsl:template>

<xsl:template match="Row">
	<tr>
		<td align="center"><b><xsl:value-of select="MILL_RP"/></b></td>
		<td align="center"><xsl:value-of select="FFBRCVD"/></td>	
		<td align="center"><xsl:value-of select="FFBPRCD"/></td>
		<td align="center"><xsl:value-of select="BALANCE"/></td>
		<td width="1000" align="center"><xsl:value-of select="OER"/></td>
		<td align="center"><xsl:value-of select="KER"/></td>
		<td align="center"><xsl:value-of select="RAINFALL"/></td>
		<td align="center"><xsl:value-of select="MILLSTART"/></td>
		<td align="center"><xsl:value-of select="MILLEND"/></td>
		<td align="center"><xsl:value-of select="THROUGHPUT"/></td>
	</tr>
</xsl:template>
</xsl:stylesheet>