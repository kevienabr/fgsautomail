<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" indent="yes" encoding="UTF-8" />
<xsl:template match="/Rows">
 <html>
	<head>
	<title>Data RS</title>
	<style>
		table {
			width:40%;
		}
		table, th, td {
			border: 1px solid black;
			border-collapse: collapse;
		}
		td, th {
			padding: 5px;
			text-align: center;
		}
	</style>
 	</head>
  	<body>
		<br/>
		<h5><i>Report Stock</i></h5>
    	<table border="1" border-collapse="collapse">
      		<tr bgcolor="lightgrey">
				<th align="center">MILL</th>
        		<th align="center">ST1 CAP</th>
        		<th align="center">ST1 VOL</th>
				<th align="center">ST1 FFA</th>
				<th align="center">PCTG1</th>
				<th align="center">ST2 CAP</th>
        		<th align="center">ST2 VOL</th>
				<th align="center">ST2 FFA</th>
				<th align="center">PCTG3</th>
				<th align="center">ST3 CAP</th>
        		<th align="center">ST3 VOL</th>
				<th align="center">ST3 FFA</th>
				<th align="center">PCTG3</th>
				<th align="center">ST4 CAP</th>
        		<th align="center">ST4 VOL</th>
				<th align="center">ST4 FFA</th>
				<th align="center">PCTG4</th>
				<th align="center">TTL STOCK</th>
				<th align="center">TTL CAP</th>
				<th align="center">PK STOCK</th>
      		</tr>
      	<xsl:apply-templates select="Row">
      	<xsl:sort select="title" />
      	</xsl:apply-templates>
    	</table>	
  	</body>
  </html>
</xsl:template>

<xsl:template match="Row">
	<tr>
		<td align="center"><b><xsl:value-of select="MILL_RS"/></b></td>
		<td align="center"><xsl:value-of select="ST1_CAP"/></td>	
		<td align="center"><xsl:value-of select="ST1_VOL"/></td>
		<td align="center"><xsl:value-of select="ST1_FFA"/></td>
		<td align="center"><xsl:value-of select="PCTG1"/></td>
		<td align="center"><xsl:value-of select="ST2_CAP"/></td>	
		<td align="center"><xsl:value-of select="ST2_VOL"/></td>
		<td align="center"><xsl:value-of select="ST2_FFA"/></td>
		<td align="center"><xsl:value-of select="PCTG2"/></td>
		<td align="center"><xsl:value-of select="ST3_CAP"/></td>	
		<td align="center"><xsl:value-of select="ST3_VOL"/></td>
		<td align="center"><xsl:value-of select="ST3_FFA"/></td>
		<td align="center"><xsl:value-of select="PCTG3"/></td>
		<td align="center"><xsl:value-of select="ST4_CAP"/></td>	
		<td align="center"><xsl:value-of select="ST4_VOL"/></td>
		<td align="center"><xsl:value-of select="ST4_FFA"/></td>
		<td align="center"><xsl:value-of select="PCTG4"/></td>
		<td align="center"><xsl:value-of select="TTL_STOCK"/></td>
		<td align="center"><xsl:value-of select="TTL_CAP"/></td>
		<td align="center"><xsl:value-of select="PK_STOCK"/></td>
	</tr>
</xsl:template>
</xsl:stylesheet>